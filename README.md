# Sublime Text 3 Preferences Files

*Student:* Depending on the course you’re taking with me, you may not need all of the preferences files included in this repository. Nonetheless, install them all and we’ll discuss which ones you may remove.

## Installation

### Mac

Place all the files ending in `.sublime-settings` into Sublime Text 3’s `User` folder. On a Mac, that folder is located in the `Packages` folder:

```
~/Library/"Application Support"/"Sublime Text 3"/Packages/
```

If the `User` folder does not exist, create it, making sure to observe case.

### Windows

In Windows 8.x/10, the `User` folder is located in…

```
C:\Users\YOUR_USERNAME\AppData\Roaming\Sublime Text 3\Packages\
```

where `YOUR_USER_NAME` is your Windows username. If the `User` folder does not exist, create it, making sure to observe case.

## Preferences File Creation Tool

For an explanation of all the entries made in the preferences files, and for help creating your own, visit [http://www.create-sublime-text-user-preferences-file.org/](http://www.create-sublime-text-user-preferences-file.org/).

Last update: *4 September 2017*

— Roy Vanegas
